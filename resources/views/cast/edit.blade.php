@extends('layout.master')

@section('title')
    <p>Halaman Edit Cast</p>
@endsection

@section('title2')
    Edit detail {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}} " method="POST">
    @csrf
    @method('PUT')
    <div class="form-row">
    <div class="form-group col-md-6">
      <label>Nama</label>
      <input type="text" name="nama" value={{$cast->nama}} class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group col-md-6">
      <label>Umur</label>
      <input type="number" name="umur" value={{$cast->umur}} class="form-control">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea name="bio" value={{$cast->bio}} class="form-control" id="" cols="30" rows="10"></textarea>
      </div>
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection