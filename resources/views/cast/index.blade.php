@extends('layout.master')

@section('title')
    <p>Daftar Cast</p>
@endsection

@section('title2')
    Daftar Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-success mb-3">Tambah Cast</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No.</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Pengaturan</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @method('delete')
                    @csrf
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
        @empty
            <tr>
                <td>Data masih kosong</td>
            </tr>
        @endforelse
    </tbody>
  </table>

@endsection