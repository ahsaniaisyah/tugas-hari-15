@extends('layout.master')

@section('title')
    <p>Detail Cast</p>
@endsection

@section('title2')
    Detail {{$cast->nama}}
@endsection

@section('content')

<h3>{{$cast->nama}} {{", "}} {{$cast->umur}} {{" tahun"}}</h3>
<p>{{$cast->bio}}</p>

@endsection