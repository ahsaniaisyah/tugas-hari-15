<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Halaman Register</title>

    </head>    
    <body>
    <h1>Buat Akun Baru!</h1>
        <h2>Sign Up Form</h2>
        <form action="/welcome" method="post">
        @csrf
        <label for="ndepan">Nama Depan:</label><br><br>
        <input type="text" name="namadepan"><br><br>

        <label for="nbelakang">Nama Belakang:</label><br><br>
        <input type="text" name="namabelakang"><br><br>

        <label for="gender">Gender:</label><br><br>
        <input type="radio" id="lk" name="gender" value="Laki-laki">
            <label for="lk">Laki-laki</label><br>
        <input type="radio" id="pr" name="gender" value="Perempuan">
            <label for="pr">Perempuan</label><br>
        <input type="radio" id="lain" name="gender" value="Lainnya">
            <label for="lain">Lainnya</label><br><br>

        <label for="warga negara">Kewarganegaraan:</label><br><br>
        <select id="warga negara">
            <option value="indonesia">Indonesia</option>
            <option value="amerika serikat">Amerika Serikat</option>
            <option value="inggris">Inggris</option>
        </select><br><br>

        <label for="bahasa">Bahasa yang digunakan:</label><br><br>
        <input type="checkbox" id="ina">
            <label for="ina">Indonesia</label><br>
        <input type="checkbox" id="eng">
            <label for="eng">English</label><br>
        <input type="checkbox" id="lain">
            <label for="lain">Lainnya</label><br>
        <br>
        
        <label for="bio">Bio:</label><br><br>
        <textarea rows="10" cols="30"></textarea><br>
        <input type="submit" value="Sign Up">
        </form>
    </body>
</html>